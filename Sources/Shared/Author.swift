//
//  Author.swift
//  backend-in-swift
//

import SwiftyJSON

public struct Author {
	
	public let id: Int
	public let firstName: String
	public let lastName: String

    public var asJson: JSON {
        var dict: [String: Any] = [:]
        dict[Keys.id.rawValue] = id
        dict[Keys.firstName.rawValue] = firstName
        dict[Keys.lastName.rawValue] = lastName
        return JSON(dict)
    }
	
	public init(id: Int, firstName: String, lastName: String) {
		self.id = id
		self.firstName = firstName
		self.lastName = lastName
	}

    public init?(json: JSON) {
        guard
            let firstName = json[Keys.firstName.rawValue].string,
            let lastName = json[Keys.lastName.rawValue].string
        else {
            return nil
        }

        if let idString = json[Keys.id.rawValue].string, let id = Int(idString) {
            self.id = id
        } else {
            self.id = json[Keys.id.rawValue].int ?? 0
        }
        self.firstName = firstName
        self.lastName = lastName
    }

    fileprivate enum Keys: String {
        case id = "id"
        case firstName = "first_name"
        case lastName = "last_name"
    }
}
