//
//  Book.swift
//  backend-in-swift
//

import SwiftyJSON

public struct Book {
	
	public let id: Int
	public let title: String
	public let author: Author

    public var asJson: JSON {
        var dict: [String: Any] = [:]
        dict[Keys.id.rawValue] = id
        dict[Keys.title.rawValue] = title
        dict[Keys.author.rawValue] = author.asJson.dictionaryObject
        return JSON(dict)
    }
	
	public init(id: Int, title: String, author: Author) {
		self.id = id
		self.title = title
		self.author = author
	}

    public init?(json: JSON) {
        guard
            let title = json[Keys.title.rawValue].string,
            let authorDictionary = json[Keys.author.rawValue].dictionaryObject,
            let author = Author(json: JSON(authorDictionary))
        else {
            return nil
        }

        if let idString = json[Keys.id.rawValue].string, let id = Int(idString) {
            self.id = id
        } else {
            self.id = json[Keys.id.rawValue].int ?? 0
        }
        self.title = title
        self.author = author
    }

    fileprivate enum Keys: String {
        case id = "id"
        case title = "title"
        case author = "author"
    }
}
