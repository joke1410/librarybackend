//
//  EnvironmentKey.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

/// Keys for environment variables.
enum EnvironmentKey: String {

    /// Key for port to host application on.
    case PORT

    /// Database's host.
    case DB_HOST

    /// Database's name.
    case DB_NAME

    /// Database's username.
    case DB_USERNAME

    /// Database username's password.
    case DB_PASSWORD
}
