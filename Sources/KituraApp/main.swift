//
//  main.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 15/03/2017.
//
//

import Foundation
import Kitura
import HeliumLogger
import Credentials
import CredentialsHTTP
import KituraSession

let router = Router()

HeliumLogger.use(.entry)

// Credentials
let credentials = Credentials()
let credentialsBasicAuth = CredentialsHTTPBasic(verifyPassword: { (user, password, userProfile) in

    let profile = HardcodedUsersStorage.shared.users[user] == password
        ? UserProfile(id: user, displayName: user, provider: "nevermind")
        : nil
    userProfile(profile)
})
credentials.register(plugin: credentialsBasicAuth)

// Session
let session = Session(secret: "kitura_session_secret")
router.all(middleware: session)
let sessionVerifier = SessionVerifier()

// Body parsing
let bodyParser = BodyParser()

// Register controllers
router.register(controller: AuthorsController(), middleware: sessionVerifier, bodyParser)
router.register(controller: BooksController(), middleware: sessionVerifier, bodyParser)
router.register(controller: LoginController(), middleware: credentials)
router.register(controller: LogoutController())

let port = Int(ProcessInfo.processInfo.environment[EnvironmentKey.PORT.rawValue] ?? "8080") ?? 8080
Kitura.addHTTPServer(onPort: port, with: router)
Kitura.run()
