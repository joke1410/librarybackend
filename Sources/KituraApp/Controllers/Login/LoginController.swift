//
//  LoginController.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 21/03/2017.
//
//

/// Controller to handle requests to `/login` endpoint.
final class LoginController: Controller {

    let endpoint = "/login"

    let operations: [Operation] = [
        LoginOperation()
    ]
}
