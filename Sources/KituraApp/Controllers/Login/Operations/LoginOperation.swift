//
//  LoginOperation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 21/03/2017.
//
//

import Kitura
import LoggerAPI
import SwiftyJSON
import Credentials
import KituraSession

struct LoginOperation: Operation {

    let method = HTTPMethod.POST

    let routerHandler: RouterHandler = { request, response, next in

        guard let id = request.userProfile?.id, let sessionState = request.session else {
            request.session?.destroy() { error in
                if let error = error {
                    Log.error("Failed to destroy session: \(error.description)")
                }
            }
            _ = response.send(status: .notFound)
            next()
            return
        }
        sessionState["user_id"] = JSON(id)
        _ = response.status(.OK).send("Session has been started!")
        next()
    }
}
