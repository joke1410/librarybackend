//
//  ListBooksOperation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import Kitura
import LoggerAPI
import Shared
import SwiftyJSON

struct ListBooksOperation: Operation {

    let method = HTTPMethod.GET

    let routerHandler: RouterHandler = { request, response, next in

        BooksProvider().provideList(filters: request.queryParameters) { books, error in
            if let error = error {
                response.send(error.localizedDescription)
                Log.error(error.localizedDescription)
            } else if let books = books {
                response.status(.OK).send(json: JSON(books.map { $0.asJson }))
            } else {
                _ = response.send(status: .unknown)
            }
            next()
        }
    }
}
