//
//  GetCoverImageOperation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 21/03/2017.
//
//

import Foundation
import Kitura
import LoggerAPI

struct GetCoverImageOperation: Operation {

    let method = HTTPMethod.GET

    let pathAddition = "/:id/cover"

    let routerHandler: RouterHandler = { request, response, next in

        guard let id = request.parameters["id"] else {
            _ = response.send(status: .badRequest)
            next()
            return
        }

        guard let coversPath = FileStorage.pathForCoverImages else {
            _ = response.send(status: .internalServerError)
            next()
            return
        }

        guard
            let existingFilePath = ["jpeg", "png"]
                .map({ coversPath + "/\(id).\($0)" })
                .first(where: { FileManager.default.fileExists(atPath: $0) })
        else {
            _ = response.send(status: .notFound)
            next()
            return
        }

        do {
            try response.send(fileName: existingFilePath)
        } catch {
            Log.error(error.localizedDescription)
            _ = response.send(status: .notFound)
        }
        next()
    }
}
