//
//  CreateBookOperation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import Kitura
import LoggerAPI
import Shared

struct CreateBookOperation: Operation {

    let method = HTTPMethod.POST

    let routerHandler: RouterHandler = { request, response, next in

        guard
            let json = request.body?.asJSON,
            let book = Book(json: json)
        else  {
            _ = response.send(status: .unprocessableEntity)
            next()
            return
        }

        BooksRequester().add(book: book) { book, error in
            if let error = error {
                response.send(error.localizedDescription)
                Log.error(error.localizedDescription)
            } else if let book = book {
                response.status(.OK).send(json: book.asJson)
            } else {
                _ = response.send(status: .unknown)
            }
            next()
        }
    }
}
