//
//  AddCoverImageOperation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 20/03/2017.
//
//

import Foundation
import Kitura
import LoggerAPI

struct AddCoverImageOperation: Operation {

    let method = HTTPMethod.POST

    let pathAddition = "/:id/cover"

    let routerHandler: RouterHandler = { request, response, next in

        guard let id = request.parameters["id"], let multipart = request.body?.asMultiPart else {
            _ = response.send(status: .badRequest)
            next()
            return
        }

        guard let coversPath = FileStorage.pathForCoverImages else {
            _ = response.send(status: .internalServerError)
            next()
            return
        }

        guard let file = multipart.first, multipart.count == 1 else {
            _ = response.status(.badRequest).send("Body is expected to contain one file of type JPEG or PNG.")
            next()
            return
        }

        let fileExtension: String? = {
            switch file.type {
            case "image/jpeg": return "jpeg"
            case "image/png": return "png"
            default: return nil
            }
        }()

        guard let imageExtension = fileExtension else {
            _ = response.status(.badRequest).send("Unsupported file's extension.")
            next()
            return
        }

        guard case .raw(let data) = file.body else {
            _ = response.status(.unprocessableEntity).send("Failed to read file's body.")
            next()
            return
        }

        do {
            try data.write(to: URL(fileURLWithPath: coversPath).appendingPathComponent("\(id).\(imageExtension)"), options: .atomic)
            _ = response.send(status: .OK)
        } catch {
            Log.error(error.localizedDescription)
            _ = response.send(status: .internalServerError)
        }

        next()
    }
}
