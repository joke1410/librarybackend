//
//  BooksController.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

/// Controller to handle requests to `/books` endpoint.
final class BooksController: Controller {

    let endpoint = "/books"

    let operations: [Operation] = [
        ListBooksOperation(),
        CreateBookOperation(),
        AddCoverImageOperation(),
        GetCoverImageOperation()
    ]
}
