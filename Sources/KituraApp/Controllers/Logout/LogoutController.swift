//
//  LogoutController.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 22/03/2017.
//
//

/// Controller to handle requests to `/logout` endpoint.
final class LogoutController: Controller {

    let endpoint = "/logout"

    let operations: [Operation] = [
        LogoutOperation()
    ]
}
