//
//  LogoutOperation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 22/03/2017.
//
//

import Kitura
import LoggerAPI
import SwiftyJSON
import Credentials
import KituraSession

struct LogoutOperation: Operation {

    let method = HTTPMethod.POST

    let routerHandler: RouterHandler = { request, response, next in

        guard let sessionState = request.session, let id = sessionState["user_id"].string else {
            _ = response.status(.OK).send("You are not logged in.")
            next()
            return
        }

        sessionState.destroy { error in
            if let error = error {
                Log.error("Failed to destoy session: \(error.localizedDescription)")
            }
        }
        _ = response.status(.OK).send("Logged out successfully!")
        next()
    }
}
