//
//  AuthorsController.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

/// Controller to handle requests to `/authors` endpoint.
final class AuthorsController: Controller {

    let endpoint = "/authors"

    let operations: [Operation] = [
        ListAuthorsOperation(),
        CreateAuthorOperation()
    ]
}
