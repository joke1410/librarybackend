//
//  CreateAuthorOperation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import Kitura
import LoggerAPI
import Shared
import SwiftyJSON

struct CreateAuthorOperation: Operation {

    let method = HTTPMethod.POST

    let routerHandler: RouterHandler = { request, response, next in

        guard
            let json = request.body?.asJSON,
            let author = Author(json: json)
        else  {
            _ = response.send(status: .unprocessableEntity)
            next()
            return
        }

        AuthorsRequester().add(author: author) { author, error in
            if let error = error {
                response.send(error.localizedDescription)
                Log.error(error.localizedDescription)
            } else if let author = author {
                response.status(.OK).send(json: author.asJson)
            } else {
                _ = response.send(status: .unknown)
            }
            next()
        }
    }
}
