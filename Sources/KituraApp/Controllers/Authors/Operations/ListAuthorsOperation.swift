//
//  ListAuthorsOperation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import Kitura
import LoggerAPI
import Shared
import SwiftyJSON

struct ListAuthorsOperation: Operation {

    let method = HTTPMethod.GET

    let routerHandler: RouterHandler = { request, response, next in

        AuthorsProvider().provideList() { authors, error in
            if let error = error {
                response.send(error.localizedDescription)
                Log.error(error.localizedDescription)
            } else if let authors = authors {
                response.status(.OK).send(json: JSON(authors.map { $0.asJson }))
            } else {
                _ = response.send(status: .unknown)
            }
            next()
        }
    }
}
