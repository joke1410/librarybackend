//
//  HardcodedUsersStorage.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 22/03/2017.
//
//

// Achtung!: Definitely this should *not* be done like that but it's not in the scope of that RnD task.
final class HardcodedUsersStorage {

    /// Holds users in dictionary "user : password".
    let users: [String: String] = [
        "john.doe@ytq.co": "awesomepassword",
        "jack.black@ytq.co": "notsoawesomepassword"
    ]

    static let shared = HardcodedUsersStorage()
}
