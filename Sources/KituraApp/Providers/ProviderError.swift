//
//  ProviderError.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 20/03/2017.
//
//

enum ProviderError: Error {
    case unknown
}
