//
//  AuthorsProvider.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import LoggerAPI
import Shared
import SwiftyJSON

final class AuthorsProvider {

    fileprivate lazy var dbAuthorsConnector = DbAuthorsConnector()

    /// Provides list of authors.
    ///
    /// - Parameter completion: Completion containing list of authors or error that is called once list is loaded.
    func provideList(completion: @escaping ([Author]?, Error?) -> Void) {
        dbAuthorsConnector.selectAuthorList { [weak self] json, error in
            if let error = error {
                Log.error(error.localizedDescription)
                completion(nil, error)
                return
            } else if let json = json {
                completion(self?.mapToAuthorList(json: json), nil)
            } else {
                Log.error(ProviderError.unknown.localizedDescription)
                completion(nil, ProviderError.unknown)
            }
        }
    }
}

fileprivate extension AuthorsProvider {

    func mapToAuthorList(json: JSON) -> [Author] {
        guard let array = json.array else { return [] }
        return array.flatMap { Author(json: $0) }
    }
}
