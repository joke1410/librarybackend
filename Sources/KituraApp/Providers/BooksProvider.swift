//
//  BooksProvider.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import LoggerAPI
import Shared
import SwiftyJSON

final class BooksProvider {

    fileprivate lazy var dbBooksConnector = DbBooksConnector()

    /// Provides list of books with optional filters.
    ///
    /// - Parameters:
    ///   - filters: Optional filters to be applied on books' list.
    ///   - completion: Completion containing list of authors or error if operation fails.
    func provideList(filters: [String: Any]? = nil, completion: @escaping ([Book]?, Error?) -> Void) {

        dbBooksConnector.selectBookList(filters: filters) { [weak self] json, error in
            if let error = error {
                Log.error(error.localizedDescription)
                completion(nil, error)
                return
            } else if let json = json {
                completion(self?.mapToBookList(json: json), nil)
            } else {
                Log.error(ProviderError.unknown.localizedDescription)
                completion(nil, ProviderError.unknown)
            }
        }
    }
}

fileprivate extension BooksProvider {

    func mapToBookList(json: JSON) -> [Book] {
        guard let array = json.array else { return [] }

        return array.flatMap { row -> Book? in

            var bookDict = [String: Any]()
            bookDict["id"] = row["id"].string
            bookDict["title"] = row["title"].string
            bookDict["author"] = {
                var authorDict = [String: Any]()
                authorDict["id"] = row["author_id"].string
                authorDict["first_name"] = row["first_name"].string
                authorDict["last_name"] = row["last_name"].string
                return authorDict
            }()

            return Book(json: JSON(bookDict))
        }
    }
}
