CREATE TABLE authors (
 id BIGSERIAL PRIMARY KEY,
 first_name varchar(100) NOT NULL CHECK (first_name <> ''),
 last_name varchar(100) NOT NULL CHECK (last_name <> '')
);

CREATE TABLE books (
  id BIGSERIAL PRIMARY KEY,
  title varchar(500) NOT NULL CHECK (title <> ''),
  author_id BIGSERIAL references authors(id)
);
