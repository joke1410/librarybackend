//
//  Operation.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import Kitura

/// Protocol that defines Operation.
/// Operation is defined with HTTP method to perform (GET/POST/PUT/DELETE), optional path addition and router handler.
protocol Operation {
    var method: HTTPMethod { get }
    var pathAddition: String { get }
    var routerHandler: RouterHandler { get }
}

extension Operation {
    var pathAddition: String {
        return ""
    }
}

enum HTTPMethod {
    case GET, POST, PUT, DELETE
}
