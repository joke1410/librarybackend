//
//  Controller.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

/// Protocol that defines Controller.
/// First of all, Controller should be described with endpoint path.
/// In addition, Controller specifies Operations that can be done on given endpoint.
protocol Controller {

    var endpoint: String { get }
    var operations: [Operation] { get }
}
