//
//  FileStorage.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 21/03/2017.
//
//

import Foundation

struct FileStorage {

    // Tricky: This is not the path where images should be stored. It's used only to demonstrate the usage of books' covers endpoint.
    static var pathForCoverImages: String? {

        let separator: Character = "/"
        let currentFilePath = #file

        var pathComponents = currentFilePath.characters.split(separator: separator).map(String.init)

        guard let indexOfSources = pathComponents.index(of: "Sources") else { return nil }

        pathComponents.removeLast(pathComponents.count - indexOfSources)

        pathComponents.append(contentsOf: ["images", "kitura", "covers"])

        return String(separator) + pathComponents.joined(separator: String(separator))
    }
}
