//
//  AuthorsRequester.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import Shared

final class AuthorsRequester {

    fileprivate lazy var dbAuthorsConnector = DbAuthorsConnector()

    /// Adds author to stored set of authors.
    ///
    /// - Parameters:
    ///   - author: Author to add.
    ///   - completion: Completion that is called once adding finishes.
    func add(author: Author, completion: @escaping (Author?, Error?) -> Void) {
        
        dbAuthorsConnector.insert(author: author) { id, error in
            guard let id = id else {
                completion(nil, error)
                return
            }

            let author = Author(id: id, firstName: author.firstName, lastName: author.lastName)

            completion(author, nil)
        }
    }
}
