//
//  RequesterError.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 24/03/2017.
//
//

enum RequesterError: Error {
    case unknown
}
