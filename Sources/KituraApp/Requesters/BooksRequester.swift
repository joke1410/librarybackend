//
//  BooksRequester.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 16/03/2017.
//
//

import Shared

final class BooksRequester {

    fileprivate lazy var dbAuthorsConnector = DbAuthorsConnector()
    fileprivate lazy var dbBooksConnector = DbBooksConnector()

    /// Adds book to stored set of books.
    ///
    /// - Parameters:
    ///   - book: Book to add.
    ///   - completion: Completion that is called once adding finishes.
    func add(book: Book, completion: @escaping (Book?, Error?) -> Void) {

        dbAuthorsConnector.insertIfNotExists(author: book.author) { [weak self] authorId, error in
            if let error = error {
                completion(nil, error)
                return
            }

            guard let authorId = authorId else {
                completion(nil, error)
                return
            }

            self?.dbBooksConnector.insert(book: book, authorId: authorId) { bookId, error in
                if let error = error {
                    completion(nil, error)
                    return
                }

                guard let bookId = bookId else {
                    completion(nil, RequesterError.unknown)
                    return
                }

                let bookToReturn = Book(
                    id: bookId,
                    title: book.title,
                    author: Author(id: authorId, firstName: book.author.firstName, lastName: book.author.lastName)
                )
                
                completion(bookToReturn, nil)
            }
        }
    }
}
