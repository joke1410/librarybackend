//
//  DbAuthorsConnector.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 17/03/2017.
//
//

import Shared
import LoggerAPI
import SwiftKuery
import SwiftKueryPostgreSQL
import SwiftyJSON

final class DbAuthorsConnector {

    /// Inserts author to database.
    ///
    /// - Parameters:
    ///   - author: Author to insert.
    ///   - completion: Completion that is called with newly inserted author's identifier or error in case operation fails.
    func insert(author: Author, completion: @escaping (Int?, Error?) -> Void) {
        let authors = Authors()

        let connection = ConnectionFactory.shared.createConnection()

        defer { connection.closeConnection() }
        connection.connect() { error in
            if let error = error {
                Log.error(error.localizedDescription)
                completion(nil, error)
                return
            } else {

                let query = Insert(
                    into: authors,
                    valueTuples:
                        (authors.firstName, author.firstName),
                        (authors.lastName, author.lastName)
                ).suffix("RETURNING \(authors.id.name)")


                connection.execute(query: query) { result in

                    if let error = result.asError {
                        Log.error(error.localizedDescription)
                        completion(nil, error)
                        return
                    }

                    guard let idString = result.asRows?.first?[authors.id.name] as? String else {
                        completion(nil, DbError.insertionUnknownError)
                        return
                    }
                    completion(Int(idString), nil)
                }
            }
        }
    }

    /// Performs insertion of author to database if does not exist.
    ///
    /// - Parameters:
    ///   - author: Author to insert.
    ///   - completion: Completion that is called with author's identifier or error in case operation fails.
    func insertIfNotExists(author: Author, completion: @escaping (Int?, Error?) -> Void) {
        let authors = Authors()

        let connection = ConnectionFactory.shared.createConnection()

        defer { connection.closeConnection() }
        connection.connect() { error in
            if let error = error {
                Log.error(error.localizedDescription)
                completion(nil, error)
                return
            } else {

                let stringQuery =
                    "WITH " +
                        "existing AS " +
                            "(SELECT \(authors.id.name) FROM \(authors.nameInQuery) WHERE \(authors.id.name) = \(author.id)), " +
                        "inserted AS " +
                            "(INSERT INTO \(authors.nameInQuery) (\(authors.firstName.name), \(authors.lastName.name)) " +
                                "SELECT '\(author.firstName)\', '\(author.lastName)' " +
                                "WHERE NOT EXISTS (SELECT 1 FROM existing) " +
                                "RETURNING \(authors.id.name)) " +
                    "SELECT \(authors.id.name) FROM inserted UNION ALL SELECT \(authors.id.name) FROM existing"

                connection.execute(stringQuery) { result in

                    if let error = result.asError {
                        Log.error(error.localizedDescription)
                        completion(nil, error)
                        return
                    }

                    guard let idString = result.asRows?.first?[authors.id.name] as? String else {
                        completion(nil, DbError.insertionUnknownError)
                        return
                    }
                    completion(Int(idString), nil)
                }
            }
        }
    }

    /// Performs selection of authors' list from database.
    ///
    /// - Parameter completion: Completion that is called with JSON or error in case operation fails.
    func selectAuthorList(completion: @escaping (JSON?, Error?) -> Void) {
        let authors = Authors()

        let connection = ConnectionFactory.shared.createConnection()

        defer { connection.closeConnection() }
        connection.connect() { error in
            if let error = error {
                Log.error(error.localizedDescription)
                completion(nil, error)
                return
            } else {

                let query = Select(authors.id, authors.firstName, authors.lastName, from: authors)

                connection.execute(query: query) { result in

                    if let rows = result.asRows {
                        let json = JSON(rows)
                        completion(json, nil)
                    } else if let queryError = result.asError {
                        Log.error(queryError.localizedDescription)
                        completion(nil, queryError)
                    } else {
                        completion(JSON([]), nil)
                    }
                }
            }
        }
    }

    /// Performs selection of author with given identifer on database.
    ///
    /// - Parameters:
    ///   - id: Identifier of author to select.
    ///   - completion: Completion that is called with JSON or error in case operation fails.
    func selectAuthor(withId id: Int, completion: @escaping (JSON?, Error?) -> Void) {
        let authors = Authors()

        let connection = ConnectionFactory.shared.createConnection()

        defer { connection.closeConnection() }
        connection.connect() { error in
            if let error = error {
                Log.error(error.localizedDescription)
                completion(nil, error)
                return
            } else {

                let query = Select(authors.id, authors.firstName, authors.lastName, from: authors).where(authors.id == id)

                connection.execute(query: query) { result in

                    if result.success {
                        if let rows = result.asRows?.first {
                            let json = JSON(rows)
                            completion(json, nil)
                        } else {
                            Log.error(DbError.notFound.localizedDescription)
                            completion(nil, DbError.notFound)
                        }
                    } else if let queryError = result.asError {
                        Log.error(queryError.localizedDescription)
                        completion(nil, queryError)
                    }
                }
            }
        }
    }
}
