//
//  DbBooksConnector.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 17/03/2017.
//
//

import Shared
import LoggerAPI
import SwiftKuery
import SwiftKueryPostgreSQL
import SwiftyJSON

class DbBooksConnector {

    /// Performs insertion of book to database.
    ///
    /// - Parameters:
    ///   - book: Book to insert.
    ///   - authorId: Identifier of author of the book.
    ///   - completion: Completion that is called with newly inserted book's identifier or error in case operation fails.
    func insert(book: Book, authorId: Int, completion: @escaping (Int?, Error?) -> Void) {
        let books = Books()

        let connection = ConnectionFactory.shared.createConnection()

        defer { connection.closeConnection() }
        connection.connect() { error in
            if let error = error {
                Log.error(error.localizedDescription)
                completion(nil, error)
                return
            } else {

                let query = Insert(
                    into: books,
                    valueTuples:
                        (books.title, book.title.replacingOccurrences(of: "'", with: "''")),
                        (books.authorId, authorId)
                ).suffix("RETURNING \(books.id.name)")


                connection.execute(query: query) { result in

                    if let error = result.asError {
                        Log.error(error.localizedDescription)
                        completion(nil, error)
                        return
                    }

                    guard let idString = result.asRows?.first?[books.id.name] as? String else {
                        completion(nil, DbError.insertionUnknownError)
                        return
                    }
                    completion(Int(idString), nil)
                }
            }
        }
    }

    /// Performs selection of books' list on database. Optional filters can be applied.
    ///
    /// - Parameters:
    ///   - filters: Optional filters to be applied on books' list.
    ///   - completion: Completion containing JSON or error if operation fails.
    func selectBookList(filters: [String: Any]? = nil, completion: @escaping (JSON?, Error?) -> Void) {
        let books = Books()
        let authors = Authors()

        var queryConditions = [(column: Column, value: Any)]()
        if let filters = filters {
            for (field, value) in filters {
                switch field {
                case books.authorId.name: queryConditions.append((books.authorId, value))
                default: break
                }
            }
        }

        let connection = ConnectionFactory.shared.createConnection()

        defer { connection.closeConnection() }
        connection.connect() { error in
            if let error = error {
                Log.error(error.localizedDescription)
                completion(nil, error)
                return
            } else {

                let query = Select(books.id, books.title, books.authorId, authors.firstName, authors.lastName, from: books).leftJoin(authors).on(books.authorId == authors.id)

                var finalQuery = query
                queryConditions.forEach {
                    finalQuery = finalQuery.where( $0.column == String(describing: $0.value))
                }

                connection.execute(query: finalQuery) { result in

                    if let rows = result.asRows {
                        let json = JSON(rows)
                        completion(json, nil)
                    } else if let queryError = result.asError {
                        Log.error(queryError.localizedDescription)
                        completion(nil, queryError)
                    } else {
                        completion(JSON([]), nil)
                    }
                }
            }
        }
    }
}
