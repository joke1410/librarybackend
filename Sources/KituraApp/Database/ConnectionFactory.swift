//
//  ConnectionFactory.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 17/03/2017.
//
//

import Foundation
import SwiftKueryPostgreSQL

final class ConnectionFactory {

    static let shared = ConnectionFactory()

    /// Creates PostgreSQL connection object.
    ///
    /// - Returns: PostgreSQL connection object
    func createConnection() -> PostgreSQLConnection {
        return PostgreSQLConnection(
            host: AppDatabaseConfig.host,
            port: AppDatabaseConfig.port,
            options: [
                .databaseName(AppDatabaseConfig.name),
                .userName(AppDatabaseConfig.username),
                .password(AppDatabaseConfig.password)
            ]
        )
    }
}
