//
//  AppDatabaseConfig.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 17/03/2017.
//
//

import Foundation

/// Holds configuration for database.
struct AppDatabaseConfig {
    static let host = ProcessInfo.processInfo.environment[EnvironmentKey.DB_HOST.rawValue] ?? "localhost"
    static let port = Int32(5432)
    static let name = ProcessInfo.processInfo.environment[EnvironmentKey.DB_NAME.rawValue] ?? "library"
    static let username = ProcessInfo.processInfo.environment[EnvironmentKey.DB_USERNAME.rawValue] ?? "postgres"
    static let password = ProcessInfo.processInfo.environment[EnvironmentKey.DB_PASSWORD.rawValue] ?? "postgres"
}
