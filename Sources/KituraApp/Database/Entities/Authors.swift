//
//  Authors.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 17/03/2017.
//
//

import SwiftKuery

class Authors : Table {
    let tableName = "authors"
    let id = Column("id")
    let firstName = Column("first_name")
    let lastName = Column("last_name")
}
