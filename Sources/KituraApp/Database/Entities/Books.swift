//
//  Books.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 17/03/2017.
//
//

import SwiftKuery

class Books : Table {
    let tableName = "books"
    let id = Column("id")
    let title = Column("title")
    let authorId = Column("author_id")
}
