//
//  DbError.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 17/03/2017.
//
//

enum DbError: Error {
    case notFound
    case insertionUnknownError
}
