//
//  SessionValidator.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 22/03/2017.
//
//

import Kitura

/// Middleware class to verify if user is logged in.
final class SessionVerifier: RouterMiddleware {

    func handle(request: RouterRequest, response: RouterResponse, next: @escaping () -> Void) throws {
        guard let sessionState = request.session, let _ = sessionState["user_id"].string else {
            try? response.status(.unauthorized).end()
            return
        }
        next()
    }
}
