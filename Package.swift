import PackageDescription

let package = Package(
	name: "LibraryBackend",
	targets: [
		Target(name: "KituraApp", dependencies: ["Shared"]),
		Target(name: "Shared"),
	],
	dependencies: [
        .Package(url: "https://github.com/IBM-Swift/Kitura.git", majorVersion: 1, minor: 6),
        .Package(url: "https://github.com/IBM-Swift/HeliumLogger.git", majorVersion: 1, minor: 6),
        .Package(url: "https://github.com/IBM-Swift/Swift-Kuery-PostgreSQL", majorVersion: 0, minor: 5),
        .Package(url: "https://github.com/IBM-Swift/Kitura-CredentialsHTTP.git", majorVersion: 1, minor: 7),
        .Package(url: "https://github.com/IBM-Swift/Kitura-Session.git", majorVersion: 1, minor: 6)
    ]
)
