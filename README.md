### Motivation

Project created in order to get known with one of backend frameworks used to write backend side in Swift. I did choose Kitura from IBM.

### Build & Run

In order to build and run project follow below steps:

- 'brew install postgresql'

- 'swift package update'

- 'swift package generate-xcodeproj'

- open Xcode and build 'KituraApp'

- run 'KituraApp'

### Sample requests

Sample requests can be found in 'LibraryBackend / Sources / KituraApp / Sample requests /'

### tables.sql

File containg script to create correct tables for the project can be found in 'LibraryBackend / Sources / KituraApp / tables.sql'