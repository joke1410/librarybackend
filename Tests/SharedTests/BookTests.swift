//
//  BookTests.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 23/03/2017.
//
//

import XCTest
import Shared
import SwiftyJSON

class BookTests: XCTestCase {

    var book: Book!

    override func tearDown() {
        book = nil
    }

    func testPositiveBookInitializationFromJSON() {

        guard let bookJsonString = bookJsonString else {
            XCTFail("Failed to read `book.json` file")
            return
        }

        let json = JSON.parse(string: bookJsonString)

        book = Book(json: json)

        XCTAssertNotNil(book)
        XCTAssertTrue(book.id == 0)
        XCTAssertTrue(book.title == "BookTitle")
        XCTAssertTrue(book.author.id == 0)
        XCTAssertTrue(book.author.firstName == "FirstName")
        XCTAssertTrue(book.author.lastName == "LastName")
    }

    func testNegativeBookInitializationFromJSON() {
        book = Book(json: JSON(""))

        XCTAssertNil(book)
    }

    func testCorrectnessOfBookJsonRepresentation() {
        book = Book(
            id: 0,
            title: "BookTitle",
            author: Author(id: 0, firstName: "FirstName", lastName: "LastName")
        )

        guard let bookJsonString = bookJsonString else {
            XCTFail("Failed to read `book.json` file")
            return
        }

        let jsonFromFile = JSON.parse(string: bookJsonString)
        let jsonFromStruct = book.asJson

        XCTAssertTrue(jsonFromFile == jsonFromStruct)
    }
}

fileprivate extension BookTests {

    var bookJsonString: String? {
        let filePath: String = {
            var components = #file.components(separatedBy: "/")
            components.removeLast(1)
            components.append("book.json")
            return components.joined(separator: "/")
        }()

        return try? String(contentsOfFile: filePath)
    }
}
