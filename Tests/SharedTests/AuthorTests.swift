//
//  AuthorTests.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 23/03/2017.
//
//

import XCTest
import Shared
import SwiftyJSON

class AuthorTests: XCTestCase {

    var author: Author!

    override func tearDown() {
        author = nil
    }

    func testPositiveAuthorInitializationFromJSON() {

        guard let authorJsonString = authorJsonString else {
            XCTFail("Failed to read `author.json` file")
            return
        }

        let json = JSON.parse(string: authorJsonString)

        author = Author(json: json)

        XCTAssertNotNil(author)
        XCTAssertTrue(author.id == 0)
        XCTAssertTrue(author.firstName == "FirstName")
        XCTAssertTrue(author.lastName == "LastName")
    }

    func testNegativeAuthorInitializationFromJSON() {
        author = Author(json: JSON(""))

        XCTAssertNil(author)
    }

    func testCorrectnessOfAuthorJsonRepresentation() {
        author = Author(id: 0, firstName: "FirstName", lastName: "LastName")

        guard let authorJsonString = authorJsonString else {
            XCTFail("Failed to read `author.json` file")
            return
        }

        let jsonFromFile = JSON.parse(string: authorJsonString)
        let jsonFromStruct = author.asJson

        XCTAssertTrue(jsonFromFile == jsonFromStruct)
    }
}

fileprivate extension AuthorTests {

    var authorJsonString: String? {
        let filePath: String = {
            var components = #file.components(separatedBy: "/")
            components.removeLast(1)
            components.append("author.json")
            return components.joined(separator: "/")
        }()

        return try? String(contentsOfFile: filePath)
    }
}
