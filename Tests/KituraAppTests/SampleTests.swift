//
//  SampleTests.swift
//  LibraryBackend
//
//  Created by Peter Bruz on 23/03/2017.
//
//

import XCTest

class SampleTests: XCTestCase {

    var x: Int!

    override func setUp() {
        super.setUp()
        x = 10
    }

    func testX() {
        XCTAssert(x == 10)
    }
}
